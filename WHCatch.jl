using XLSX
using Dates
using DataFrames
using Printf
using OffsetArrays

include("Types.jl")

catchment = Nothing
evap = Nothing
prec = Nothing
inflowFrom = Nothing
orderOfCalculation = Nothing
balance = Nothing
total = Nothing

infiltrationTime = 4.0
factorForSmax = 0.9

outputVx = Nothing
outputSmax = Nothing
outputVin = Nothing
outputVinf = Nothing
outputVs = Nothing
outputdV = Nothing
outputRunoff = Nothing

yearToShow = 1992
firstYear = 1980
lastYear = 2100

function createCatchment(aData :: DataFrame)
  global catchment = Array{Types.CatchmentData}(undef,1)
  try
    try
      resize!(catchment, size(aData,1)-1)
      for i in 2:size(aData,1)
        id  = -1
        if !ismissing(aData[i,1])
          if typeof(aData[i,1]) == String
            if strip(aData[i,1]) != ""
              id = tryparse(Int64, aData[i,1])
            end
          else
            id = aData[i,1]
          end
        end

        area = -1.0
        if !ismissing(aData[i,2])
          if typeof(aData[i,2]) == String
            if strip(aData[i,2]) != ""
              area = tryparse(Float64, aData[i,2])
            end
          else
            area = aData[i,2]
          end
        end

        cultivated = -1.0
        if !ismissing(aData[i,3])
          if typeof(aData[i,3]) == String
            if strip(aData[i,3]) != ""
              cultivated = tryparse(Float64, aData[i,3])
            end
          else
            cultivated = aData[i,3]
          end
        end

        storage = -1.0
        if !ismissing(aData[i,4])
          if typeof(aData[i,4]) == String
            if strip(aData[i,4]) != ""
              storage = tryparse(Float64, aData[i,4])
            end
          else
            storage = aData[i,4]
          end
        end

        maxHeight = -1.0
        if !ismissing(aData[i,5])
          if typeof(aData[i,5]) == String
            if strip(aData[i,5]) != ""
              maxHeight = tryparse(Float64, aData[i,5])
            end
          else
            maxHeight = aData[i,5]
          end
        end

        runoffCoefficient = -1.0
        if !ismissing(aData[i,6])
          if typeof(aData[i,6]) == String
            if strip(aData[i,6]) != ""
              runoffCoefficient = tryparse(Float64, aData[i,6])
            end
          else
            runoffCoefficient = aData[i,6]
          end
        end

        infiltrationRate = -1.0
        if !ismissing(aData[i,7])
          if typeof(aData[i,7]) == String
            if strip(aData[i,7]) != ""
              infiltrationRate = tryparse(Float64, aData[i,7])
            end
          else
            infiltrationRate = aData[i,7]
          end
        end

        infiltrationPercentage = -1.0
        if !ismissing(aData[i,8])
          if typeof(aData[i,8]) == String
            if strip(aData[i,8]) != ""
              infiltrationPercentage = tryparse(Float64, aData[i,8])
            end
          else
            infiltrationPercentage = aData[i,8]
          end
        end

        outflowsTo1 = -1.0
        if !ismissing(aData[i,9])
          if typeof(aData[i,9]) == String
            if strip(aData[i,9]) != ""
              outflowsTo1 = tryparse(Int64, aData[i,9])
            end
          else
            outflowsTo1 = aData[i,9]
          end
        end

        percentage1 = -1.0
        if !ismissing(aData[i,10])
          if typeof(aData[i,10]) == String
            if strip(aData[i,10]) != ""
              percentage1 = tryparse(Float64, aData[i,10])
            end
          else
            percentage1 = aData[i,10]
          end
        end

        outflowsTo2 = -1.0
        if !ismissing(aData[i,11])
          if typeof(aData[i,11]) == String
            if strip(aData[i,11]) != ""
              outflowsTo2 = tryparse(Int64, aData[i,11])
            end
          else
            outflowsTo2 = aData[i,11]
          end
        end

        percentage2 = -1.0
        if !ismissing(aData[i,12])
          if typeof(aData[i,12]) == String
            if strip(aData[i,12]) != ""
              percentage2 = tryparse(Float64, aData[i,12])
            end
          else
            percentage2 = aData[i,12]
          end
        end

        global catchment[i-1] = Types.CatchmentData(id,area,cultivated,storage,maxHeight,runoffCoefficient,
                                       infiltrationRate,infiltrationPercentage,outflowsTo1,
                                       percentage1,outflowsTo2,percentage2)
      end
    catch e
      println("???Error in createCatchment: ",e)
    end
  finally
  end
end

function createETp(aData :: DataFrame)
  global evap = Array{Types.Evapotranspiration}(undef,1)
  try
    try
      resize!(evap, size(aData,1)-1)
      for i in 2:size(aData,1)
        year  = -1
        if !ismissing(aData[i,1])
          if typeof(aData[i,1]) == String
            if strip(aData[i,1]) != ""
              year = tryparse(Int64, aData[i,1])
            end
          else
            year = aData[i,1]
          end
        end

        etp = -1.0
        if !ismissing(aData[i,2])
          if typeof(aData[i,2]) == String
            if strip(aData[i,2]) != ""
              etp = tryparse(Float64, aData[i,2])
            end
          else
            etp = aData[i,2]
          end
        end

        global evap[i-1] = Types.Evapotranspiration(year, etp)
      end
    catch e
      println("???Error in createETp: ",e)
    end
  finally
  end
end

function createPrecipitationData(aData :: DataFrame)
  try
    try
      yearFirst = -1
      if !ismissing(aData[3,1])
        if typeof(aData[3,1]) == String
          if strip(aData[3,1]) != ""
            yearFirst = tryparse(Int64, aData[3,1])
          end
        else
          yearFirst = aData[3,1]
        end
      end
      yearLast = -1
      n = size(aData,1)
      while n > 2 && yearLast < 0
        if !ismissing(aData[n,1])
          if typeof(aData[n,1]) == String
            if strip(aData[n,1]) != ""
              yearLast = tryparse(Int64, aData[n,1])
            end
          else
            yearLast = aData[n,1]
          end
        else
          n -= 1
        end
      end
      global prec = OffsetArray{Types.Precipitation}(undef,yearFirst:yearLast)
      for i in 3:size(aData,1)
        year  = -1
        if !ismissing(aData[i,1])
          if typeof(aData[i,1]) == String
            if strip(aData[i,1]) != ""
              year = tryparse(Int64, aData[i,1])
            end
          else
            year = aData[i,1]
          end
        end
        leap = isleapyear(Date(year,1,1))
        ev = Array{Float64}(undef,1)
        p = Types.Precipitation(year, 0.0, ev)
        p.year = year
        days = 365
        if leap
          days = 366
        end
        resize!(p.event, days)
        for j in 2:days+1
          event = -1.0
          if !ismissing(aData[i,j])
            if typeof(aData[i,j]) == String
              if strip(aData[i,j]) != ""
                event = tryparse(Float64, aData[i,j])
              end
            else
              event = aData[i,j]
            end
          end
          p.event[j-1] = event
          if event > 0.0
            p.total += event
          end
        end
        global prec[year] = p
      end
    catch e
      println("???Error in createPrecipitationData: ",e)
    end
  finally
  end
end

function readData()
  fileName = "/home/wesseling/DataDisk/Wesseling/Work/Ammar/Original/WHCatch2.xlsm"
  try
    try
      xf = XLSX.readxlsx(fileName)
      sheet = xf["Catchment"]
      data = dropmissing!(DataFrame(sheet[:]))
      createCatchment(data)
      sheet = xf["ETp"]
      data = dropmissing!(DataFrame(sheet[:]))
      createETp(data)
      sheet = xf["Rainfall"]
      data = DataFrame(sheet[:])
      createPrecipitationData(data)
    catch e
      println("???ERROR in readData: ",e)
    end
  finally
  end
end

function determineInflow()
  try
    try
      global inflowFrom = Array{Types.InflowFrom}(undef,1)
      n = size(catchment,1)
      resize!(inflowFrom,n)
      for i in 1:n
        x = Types.Inflow(-1,-1,0.0)
        arr = Array{Types.Inflow}(undef,1)
        arr[1] = x
        tmp = Types.InflowFrom(0,arr)
        inflowFrom[i] = tmp
      end
      for i in 1:n
        if catchment[i].outflowTo1 > 0 && catchment[i].percentage1 > 0.0
          global inflowFrom[catchment[i].outflowTo1].number += 1
          resize!(inflowFrom[catchment[i].outflowTo1].subcatchment, inflowFrom[catchment[i].outflowTo1].number)
          tmp = Types.Inflow(i,1,0.01*catchment[i].percentage1)
          global inflowFrom[catchment[i].outflowTo1].subcatchment[inflowFrom[catchment[i].outflowTo1].number] = tmp
        end
        if catchment[i].outflowTo2 > 0 && catchment[i].percentage2 > 0.0
          global inflowFrom[catchment[i].outflowTo2].number += 1
          resize!(inflowFrom[catchment[i].outflowTo2].subcatchment, inflowFrom[catchment[i].outflowTo2].number)
          tmp = Types.Inflow(i,1,0.01*catchment[i].percentage2)
          global inflowFrom[catchment[i].outflowTo2].subcatchment[inflowFrom[catchment[i].outflowTo2].number] = tmp
        end
      end
    catch e
      println("???ERROR in determineInflow: ", e)
    end
  finally
  end
end

function findOrder()
  try
    try
      n = size(catchment,1)
      global orderOfCalculation = Array{Int64}(undef,n)
      selected = Array{Bool}(undef,n)
      for i in 1:n
        global orderOfCalculation[i] = -1
        selected[i] = false
      end

      number = 0
#     subcatchments without inflow
       for i in 1:n
         if inflowFrom[i].number <= 0
           number += 1
           selected[i] = true
           global orderOfCalculation[number] = i
         end
       end

#     the others
      iter = 0
      maxIter = 2 * n
      while number < n && iter < maxIter
        iter += 1
        for i in 1:n
          if !selected[i]
            ok = true
            for j in 1:inflowFrom[i].number
              if !selected[inflowFrom[i].subcatchment[j].from]
                ok = false
                break
              end
            end
            if ok
              number += 1
              global orderOfCalculation[number] = i
              selected[i] = true
            end # if ok
          end # if !selected
        end # for i
      end # while
    catch e
      println("???ERROR in findOrder: ",e)
    end
  finally
  end
end

function zeroBalance(aBalance :: Types.Balance)
  aBalance.fromNonStorage = 0.0
  aBalance.intoStorage = 0.0
  aBalance.infiltration = 0.0
  aBalance.inflow = 0.0
  aBalance.totalIntoStorage = 0.0
  aBalance.storage = 0.0
  aBalance.maxStorage = 0.0
  aBalance.outflow = 0.0
end

function createBalance()
  try
    try
      n = size(catchment,1)
      global balance = Array{Types.Balance}(undef, n)
      global total = Array{Types.Balance}(undef,n)
      for i in 1:n
        myBalance = Types.Balance(0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0)
        global balance[i] = myBalance
        zeroBalance(balance[i])
        myBalance = Types.Balance(0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0)
        global total[i] = myBalance
        zeroBalance(total[i])
      end
    catch e
      println("???ERROR in createBalance: ",e)
    end
  finally
  end
end

function initialize()
  readData()
  determineInflow()
  findOrder()
  createBalance()
end

function balanceOfSubcatchment(aPrecipitation :: Float64, aSubcatchment :: Int64)
  try
    try
      myBalance = Types.Balance(0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0)
      myBalance.fromNonStorage = aPrecipitation * catchment[aSubcatchment].runoffCoefficient *
                                (catchment[aSubcatchment].area - catchment[aSubcatchment].storage)
      myBalance.intoStorage = aPrecipitation * catchment[aSubcatchment].storage

      myBalance.inflow = 0.0
      if inflowFrom[aSubcatchment].number > 0
        for i in 1:inflowFrom[aSubcatchment].number
          myBalance.inflow += balance[inflowFrom[aSubcatchment].subcatchment[i].from].outflow *
                              inflowFrom[aSubcatchment].subcatchment[i].fraction
        end
      end
      myBalance.totalIntoStorage = myBalance.fromNonStorage + myBalance.intoStorage + myBalance.inflow
      myBalance.infiltration = 0.001 * catchment[aSubcatchment].infiltrationRate * catchment[aSubcatchment].storage * infiltrationTime
      if myBalance.infiltration > myBalance.totalIntoStorage
        myBalance.infiltration = myBalance.totalIntoStorage
      end
      myBalance.storage = myBalance.totalIntoStorage - myBalance.infiltration
      myBalance.maxStorage = factorForSmax * catchment[aSubcatchment].storage * catchment[aSubcatchment].maxHeight
      myBalance.outflow = 0.0
      if myBalance.storage > myBalance.maxStorage
        myBalance.outflow = myBalance.storage - myBalance.maxStorage
        myBalance.storage = myBalance.maxStorage
      end
      global balance[aSubcatchment] = myBalance
    catch e
      println("???ERROR in balanceOfSubcatchment: ",e)
    end
  finally
  end
end

function processDailyValues(aPrecipitation :: Float64)
  try
    try
      prec = 0.001 * aPrecipitation
      for i in 1:size(orderOfCalculation,1)
        mySub = orderOfCalculation[i]
        balanceOfSubcatchment(aPrecipitation, mySub)
      end
    catch e
      println("???ERROR in processDailyValue: ",e)
    end
  finally
  end
end

function setTotalToZero()
  try
    try
      for i in 1:size(total,1)
        zeroBalance(total[i])
      end
    catch e
      println("???ERROR in setTotalToZero: ",e)
    end
  finally
  end
end

function addToTotal()
  try
    try
      for i in 1:size(balance,1)
        global total[i].fromNonStorage += balance[i].fromNonStorage
        global total[i].intoStorage += balance[i].intoStorage
        global total[i].infiltration += balance[i].infiltration
        global total[i].inflow += balance[i].inflow
        global total[i].totalIntoStorage += balance[i].totalIntoStorage
        global total[i].storage += balance[i].storage
        global total[i].maxStorage = balance[i].maxStorage
        global total[i].outflow += balance[i].outflow
      end
    catch e
      println("???ERROR in addToTotal: ", e)
    end
  finally
  end
end

function prepareOutput()
  try
    try
      n = 0
      if yearToShow > 0
        n = 365
        if yearToShow % 4 == 0
          n = 366
        end
      else
        n = lastYear - firstYear + 1
      end
      global outputVx = Array{Types.outputRecord}(undef,n)
      global outputSmax = Array{Types.outputRecord}(undef,n)
      global outputVin  = Array{Types.outputRecord}(undef,n)
      global outputVinf = Array{Types.outputRecord}(undef,n)
      global outputVs = Array{Types.outputRecord}(undef,n)
      global outputdV = Array{Types.outputRecord}(undef,n)
      global outputRunoff = Array{Types.outputRecord}(undef,n)
    catch e
      println("???ERROR in prepareOutput: ", e)
    end
  finally
  end
end

function selectOutputPerDay(aDay :: Int64)
  try
    try
      n = size(catchment,1)

      out = Array{Float64}(undef,n)
      for j in 1:n
        out[j] = balance[j].inflow
      end
      global outputVx[aDay] = Types.outputRecord(aDay, out)

      out = Array{Float64}(undef,n)
      for j in 1:n
        out[j] = balance[j].maxStorage
      end
      global outputSmax[aDay] = Types.outputRecord(aDay, out)

      out = Array{Float64}(undef,n)
      for j in 1:n
        out[j] = balance[j].fromNonStorage
      end
      global outputVin[aDay] = Types.outputRecord(aDay, out)

      out = Array{Float64}(undef,n)
      for j in 1:n
        out[j] = balance[j].infiltration
      end
      global outputVinf[aDay] = Types.outputRecord(aDay, out)

      out = Array{Float64}(undef,n)
      for j in 1:n
        out[j] = balance[j].intoStorage
      end
      global outputVs[aDay] = Types.outputRecord(aDay, out)

      out = Array{Float64}(undef,n)
      for j in 1:n
        out[j] = balance[j].storage
      end
      global outputdV[aDay] = Types.outputRecord(aDay, out)

      out = Array{Float64}(undef,n)
      for j in 1:n
        out[j] = balance[j].outflow
      end
      global outputRunoff[aDay] = Types.outputRecord(aDay, out)

    catch e
      println("???ERROR in selectOutputPerDay: ", e)
    end
  finally
  end
end

function selectOutputPerYear(aYear :: Int64)
  try
    try
      recNr = aYear - firstYear + 1
      n = size(balance,1)
      out = Array{Float64}(undef,n)

      for i in 1:n
        out[i] = total[i].inflow
      end
      global outputVx[recNr] = Types.outputRecord(aYear, out)

      out = Array{Float64}(undef,n)
      for i in 1:n
        out[i] = total[i].maxStorage
      end
      global outputSmax[recNr] = Types.outputRecord(aYear, out)

      out = Array{Float64}(undef,n)
      for i in 1:n
        out[i] = total[i].fromNonStorage
      end
      global outputVin[recNr] = Types.outputRecord(aYear, out)

      out = Array{Float64}(undef,n)
      for i in 1:n
        out[i] = total[i].infiltration
      end
      global outputVinf[recNr] = Types.outputRecord(aYear, out)

      out = Array{Float64}(undef,n)
      for i in 1:n
        out[i] = total[i].intoStorage
      end
      global outputVs[recNr] = Types.outputRecord(aYear, out)

      out = Array{Float64}(undef,n)
      for i in 1:n
        out[i] = total[i].storage
      end
      global outputdV[recNr] = Types.outputRecord(aYear, out)

      out = Array{Float64}(undef,n)
      for i in 1:n
        out[i] = total[i].outflow
      end
      global outputRunoff[recNr] = Types.outputRecord(aYear, out)

    catch e
      println("???ERROR in selectOutputPerYear: ", e)
    end
  finally
  end
end

function saveOutputToFile()
  try
    try
      m = 366
      n = size(catchment,1)
      if yearToShow > 0
        m = size(catchment,1)
        n = 365
        if yearToShow % 4 == 0
          n = 366
        end
      end

#     Vout
      line = "Vout"
      for i in 1:m+1
        line *= ", "
      end
      line *= "\n,P"

      for i in 1:m
        line *= "," * string(i)
      end
      line *= "\n"

      for i in 1:n
        line *= string(outputVx[i].year)
        if yearToShow == 0
          line *= "," * @sprintf("%.1f",prec[outputVx[i].year].total)
        else
          line *= "," * @sprintf("%.2f",prec[yearToShow].event[i])
        end
        for j in 1:size(outputVx[i].value,1)
          line *= "," * @sprintf("%.5e",outputVx[i].value[j])
        end
        line *= "\n"
      end

      out = open("/home/wesseling/DataDisk/Wesseling/Work/Ammar/whcatch/csv/outputVx.csv", "w")
      write(out, line)
      close(out)

#     Smax
      line = "Smax"
      for i in 1:m+1
        line *= ", "
      end
      line *= "\n,P"

      for i in 1:m
        line *= "," * string(i)
      end
      line *= "\n"

      for i in 1:n
        line *= string(outputSmax[i].year)
        if yearToShow == 0
          line *= "," * @sprintf("%.1f",prec[outputSmax[i].year].total)
        else
          line *= "," * @sprintf("%.2f",prec[yearToShow].event[i])
        end
        for j in 1:size(outputVx[i].value,1)
          line *= "," * @sprintf("%.5e",outputSmax[i].value[j])
        end
        line *= "\n"
      end

      out = open("/home/wesseling/DataDisk/Wesseling/Work/Ammar/whcatch/csv/outputSmax.csv", "w")
      write(out, line)
      close(out)

#     Vin
      line = "Vin"
      for i in 1:m+1
        line *= ", "
      end
      line *= "\n,P"

      for i in 1:m
        line *= "," * string(i)
      end
      line *= "\n"

      for i in 1:n
        line *= string(outputVin[i].year)
        if yearToShow == 0
          line *= "," * @sprintf("%.1f",prec[outputVin[i].year].total)
        else
          line *= "," * @sprintf("%.2f",prec[yearToShow].event[i])
        end
        for j in 1:size(outputVin[i].value,1)
          line *= "," * @sprintf("%.5e",outputVin[i].value[j])
        end
        line *= "\n"
      end

      out = open("/home/wesseling/DataDisk/Wesseling/Work/Ammar/whcatch/csv/outputVin.csv", "w")
      write(out, line)
      close(out)

#     Vinf
      line = "Vinf"
      for i in 1:m+1
        line *= ", "
      end
      line *= "\n,P"

      for i in 1:m
        line *= "," * string(i)
      end
      line *= "\n"

      for i in 1:n
        line *= string(outputVinf[i].year)
        if yearToShow == 0
          line *= "," * @sprintf("%.1f",prec[outputVinf[i].year].total)
        else
          line *= "," * @sprintf("%.2f",prec[yearToShow].event[i])
        end
        for j in 1:size(outputVinf[i].value,1)
          line *= "," * @sprintf("%.5e",outputVinf[i].value[j])
        end
        line *= "\n"
      end

      out = open("/home/wesseling/DataDisk/Wesseling/Work/Ammar/whcatch/csv/outputVinf.csv", "w")
      write(out, line)
      close(out)

#     Vs
      line = "Vs"
      for i in 1:m+1
        line *= ", "
      end
      line *= "\n,P"

      for i in 1:m
        line *= "," * string(i)
      end
      line *= "\n"

      for i in 1:n
        line *= string(outputVs[i].year)
        if yearToShow == 0
          line *= "," * @sprintf("%.1f",prec[outputVs[i].year].total)
        else
          line *= "," * @sprintf("%.2f",prec[yearToShow].event[i])
        end
        for j in 1:size(outputVs[i].value,1)
          line *= "," * @sprintf("%.5e",outputVs[i].value[j])
        end
        line *= "\n"
      end

      out = open("/home/wesseling/DataDisk/Wesseling/Work/Ammar/whcatch/csv/outputVs.csv", "w")
      write(out, line)
      close(out)

#     dV
      line = "Vs"
      for i in 1:m+1
        line *= ", "
      end
      line *= "\n,P"

      for i in 1:m
        line *= "," * string(i)
      end
      line *= "\n"

      for i in 1:n
        line *= string(outputdV[i].year)
        if yearToShow == 0
          line *= "," * @sprintf("%.1f",prec[outputdV[i].year].total)
        else
          line *= "," * @sprintf("%.2f",prec[yearToShow].event[i])
        end
        for j in 1:size(outputdV[i].value,1)
          line *= "," * @sprintf("%.5e",outputVs[i].value[j])
        end
        line *= "\n"
      end

      out = open("/home/wesseling/DataDisk/Wesseling/Work/Ammar/whcatch/csv/outputdV.csv", "w")
      write(out, line)
      close(out)

#     Runoff
      line = "Runoff"
      for i in 1:m+1
        line *= ", "
      end
      line *= "\n,P"

      for i in 1:m
        line *= "," * string(i)
      end
      line *= "\n"

      for i in 1:n
        line *= string(outputRunoff[i].year)
        if yearToShow == 0
          line *= "," * @sprintf("%.1f",prec[outputRunoff[i].year].total)
        else
          line *= "," * @sprintf("%.2f",prec[yearToShow].event[i])
        end
        for j in 1:size(outputRunoff[i].value,1)
          line *= "," * @sprintf("%.5e",outputRunoff[i].value[j])
        end
        line *= "\n"
      end

      out = open("/home/wesseling/DataDisk/Wesseling/Work/Ammar/whcatch/csv/outputRunoff.csv", "w")
      write(out, line)
      close(out)

    catch e
      println("???ERROR in saveOutputToFile: ", e)
    end
  finally
  end
end

function process()
  try
    try
      prepareOutput()
      if yearToShow > 0
        global firstYear = yearToShow
        global lastYear = yearToShow
      end
      for i in first(axes(prec,1)):last(axes(prec,1))
        if prec[i].year >= firstYear && prec[i].year <= lastYear
          setTotalToZero()
          for j in 1:size(prec[i].event,1)
            processDailyValues(prec[i].event[j])
            if yearToShow > 0
              selectOutputPerDay(j)
            else
              addToTotal()
            end
          end
          if yearToShow == 0
            selectOutputPerYear(prec[i].year)
          end
        end
      end
      saveOutputToFile()
    catch e
      println("???ERROR in process: ",e)
    end
  finally
  end
end


initialize()
process()


println("Finished")
