module Types
  mutable struct CatchmentData
    id :: Int64
    area :: Float64
    cultivated :: Float64
    storage :: Float64
    maxHeight :: Float64
    runoffCoefficient :: Float64
    infiltrationRate :: Float64
    infiltrationPercentage :: Float64
    outflowTo1 :: Int64
    percentage1 :: Float64
    outflowTo2 :: Int64
    percentage2 :: Float64
  end

  mutable struct Evapotranspiration
    year :: Int64
    evapotranspiration :: Float64
  end

  mutable struct Precipitation
    year :: Int64
    total :: Float64
    event :: Array{Float64}
  end

  mutable struct Inflow
    from :: Int64
    oneOrTwo :: Int64
    fraction :: Float64
  end

  mutable struct InflowFrom
    number :: Int64
    subcatchment :: Array{Inflow}
  end

  mutable struct Balance
    fromNonStorage :: Float64
    intoStorage :: Float64
    infiltration :: Float64
    inflow :: Float64
    totalIntoStorage :: Float64
    storage :: Float64
    maxStorage :: Float64
    outflow :: Float64
  end

  mutable struct outputRecord
    year :: Int64
    value :: Array{Float64}
  end

end
